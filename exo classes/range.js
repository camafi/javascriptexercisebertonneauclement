const assert = require("assert")

class Range {
    #from;
    #to;

    constructor(from,to){
        this.#from = from;
        this.#to = to;
    };

    includes(x){
        if(x < this.#to && x > this.#from){
            return true;
        }else{
            return false;
        }
    };

    toString(){
        return (this.#from + "..." + this.#to)
    };

    static parse(s){
        return new Range(s[0],s[s.length-1])
    };

    static integerRangePattern = new RegExp("[0-9]+[.]{3}[0-9]+")
    ;
}

const c = new Range(1,5);
console.log(c.includes(3))
console.log(c.includes(8))
console.log(c.toString())
console.log(integerRangePattern = "1...5" )
assert.deepEqual(c.includes(3),true)
assert.deepEqual(c.includes(8),false)
assert.deepEqual(c.toString(),"1...5")
assert.deepEqual(integerRangePattern,"1...5")


class Span extends Range{

    constructor(from,span){
        super(from,from + span);
    }
}

const s = new Span(9,-5)
console.log(s.toString())
assert.deepEqual(s.toString(),"9...4")
