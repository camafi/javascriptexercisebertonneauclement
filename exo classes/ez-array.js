const assert = require("assert")
class EZArray extends Array{

    get first(){ return this[0]}
    get last(){ return this[this.length-1]}

}

let a = new EZArray();
console.log(a instanceof EZArray);
assert.deepEqual(a instanceof EZArray,true)


console.log(a instanceof Array);
assert.deepEqual(a instanceof Array,true)


a.push(1,2,3,4)
assert.deepEqual(a.pop(),4)

assert.deepEqual(a.first,1)
console.log(a.first);

console.log(a.last);
assert.deepEqual(a.last,3)

assert.deepEqual(a[1],2)
console.log(a[1]);


console.log(Array.isArray(a))
assert.deepEqual(Array.isArray(a),true)

console.log(EZArray.isArray(a))
assert.deepEqual(EZArray.isArray(a),true)
