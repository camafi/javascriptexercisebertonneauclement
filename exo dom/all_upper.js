function recursiveTreee(node) {
    if (node.nodeType === 3) {
        node.data = node.data.toUpperCase()
    } else if (node.hasChildNodes()) {
        node.childNodes.forEach(node => {
            recursiveTreee(node)
        });
    }
}
document.childNodes.forEach(e => {
    recursiveTreee(e)
})