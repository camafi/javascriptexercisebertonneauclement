const assert = require("assert")
function split(carac,sep){
    let trier = [];
    let start = 0;
    for(let i =0; i < carac.length;i++){
        if(carac.substr(i,sep.length) === sep){
            trier.push(carac.substr(start,i-start));
            //console.log(carac.substr(start,i))

            start = i+sep.length;
        }
        if(i === carac.length-1)trier.push(carac.substr(start,i))
    }
    return trier
}

let result = split("salutbecommentbetubevasbetoi","be")
console.log(result)
assert.deepEqual(split("salutbecommentbetubevasbetoi","be"),[ 'salut', 'comment', 'tu', 'vas', 'toi' ])