const assert = require("assert")
function contains(haystack,needles,startIndex){
    if(startIndex=== undefined){
        startIndex = 0;
    }
    for(let i = startIndex;i<haystack.length;i++){
        if(haystack.substr(i,needles.length)=== needles){
            return i;
        }
    }
    return -1
}

console.log(contains("salut comment tu vas toi","tu"))
assert.deepEqual(contains("salut comment tu vas toi","tu"),14)