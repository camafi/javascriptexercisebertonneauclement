const assert = require("assert")
function compose(jsaispas,lautre){
    return value => jsaispas(value) + lautre(value)
}

const multiplie = x => x * 2;
const addition = x => x + 1;

const operations = compose(multiplie,addition)

console.log(operations(3))
assert.deepEqual(operations(3),10)