const assert = require("assert")
function padRight(char,str,quantity){
    if(str.length > quantity){
        str = str.substr(0,quantity)
        return str
    }else if(str.length === quantity){
        return str
    }else{
        return{
            right : function(){
                let result = "";
                let need = quantity - str.length
                for(let i = 0 ; i<need;i++){
                    if(i === 0){
                        result += str;
                    }
                    result += char;
                    if(i === need-1){
                        return result;
                    }
                    
                }
            }
        }
        
    }
}

console.log(padRight("0","salut",8).right())
assert.deepEqual(padRight("0","salut",8).right(),"salut000")