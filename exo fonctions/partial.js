const assert = require("assert")
function partial(fonction,nombres){

    //fonction.bind(undefined,nombres)

    return (value,yo) => fonction(nombres,value,yo)
    
}

const f = (x,y,z) => x * (y-z);
console.log(partial(f,5)(4,2))
assert.deepEqual(f(5,4,2),10)