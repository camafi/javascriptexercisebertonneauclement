const assert = require("assert")
function hanoi(disques,depart,arriver){
    if (disques == 1) {
        console.log(depart, '->', arriver);
    } else {
        let other = 6 - (depart + arriver);
        hanoi(disques - 1, depart, other);
        console.log(depart, '->', arriver);
        hanoi(disques - 1, other, arriver);
    }
}
hanoi(3, 1, 3);
