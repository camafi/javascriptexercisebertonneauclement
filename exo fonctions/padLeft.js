const assert = require("assert")
function padLeft(char,str,quantity){
    if(str.length > quantity){
        str = str.substr(0,quantity)
        return str
    }else if(str.length === quantity){
        return str
    }else{
        let result = ""
        let need = quantity - str.length
        for(let i = 0 ; i<need;i++){
            result += char;
            if(i === need - 1){
                result += str;
                return result;
            }
        }

    }
}
console.log(padLeft("l","salut",18))

const padZeros = padLeft.bind(undefined,"0");
console.log(padZeros("salut",10));
assert.deepEqual(padLeft("l","salut",18),"lllllllllllllsalut");
assert.deepEqual(padZeros("salut",10),"00000salut")