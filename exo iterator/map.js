const assert = require("assert")

function map(iterable,f){
    const iterator = iterable[Symbol.iterator]();
    let result = iterator.next();
    let array = [];
    while(!result.done){
        array.push(f(result.value))
        result = iterator.next()
    }
    return array
}

const helloIterable = map("hello",v => v.toUpperCase())
const iterator = helloIterable[Symbol.iterator]();
assert.deepEqual(iterator.next(),{ value: 'H', done: false })
assert.deepEqual(iterator.next(),{ value: 'E', done: false })
assert.deepEqual(iterator.next(),{ value: 'L', done: false })
assert.deepEqual(iterator.next(),{ value: 'L', done: false })
assert.deepEqual(iterator.next(),{ value: 'O', done: false })
assert.deepEqual(iterator.next(),{ value: undefined, done: true })
