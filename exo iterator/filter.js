const assert = require("assert")

function filter(iterable,f){
    const iterator = iterable[Symbol.iterator]();
    let result = iterator.next();
    let array = [];
    while(!result.done){
        if(f(result.value) === false){
            array.push(result.value)
        }
        result = iterator.next()
    }
    return array
}

const helloIterable = filter("hello",v => "salut".includes(v))
const iterator = helloIterable[Symbol.iterator]();

assert.deepEqual(iterator.next(),{ value: 'h', done: false })
assert.deepEqual(iterator.next(),{ value: 'e', done: false })
assert.deepEqual(iterator.next(),{ value: 'o', done: false })
assert.deepEqual(iterator.next(),{ value: undefined, done: true })
