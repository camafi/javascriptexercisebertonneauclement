const assert = require("assert")

function* zip(...iterables) {
    let results = [];
    for(iterable of iterables){
        results.push(iterable[Symbol.iterator]())
    }

    do{
        y = 0;
        for(let result of results){
            let value = result.next()
            if(!value.done){
                yield value.value
            }else{
                y++
            }
        }

    } while(y !== results.length)
    
}


assert.deepEqual([...zip("abc", [1, 2, 3])], ['a', 1, 'b', 2, 'c', 3])
assert.deepEqual([...zip("abcd", "123", "+-")], ['a', '1', '+', 'b', '2', '-', 'c', '3', 'd'])
