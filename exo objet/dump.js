const assert = require("assert")

function dump(obj){
    let result = "{";

    for(const property in obj){
        result += property + ":"
        if(typeof obj[property] === 'string'){
            result += '"' + obj[property] + '",';
        }else if (Array.isArray(obj[property])){
            result += "["
            for(let i = 0; i < obj[property].length;i++){
                if(i === obj[property].length -1){
                    result += obj[property][i]
                }else{
                    result += obj[property][i] + ","
                }
                
            }
            result += "]"
        }  
    }
    result += "}"
    return result;

}

let obj = {}
obj.firstname = "Alan"
obj.lastname = "Nala"
obj.birthday = [28,10,2000]
console.log(dump(obj))
assert.deepEqual(dump(obj), '{firstname:"Alan",lastname:"Nala",birthday:[28,10,2000]}')
