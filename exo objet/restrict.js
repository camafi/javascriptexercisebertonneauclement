const assert = require("assert")

function restrict(target,keep){
    let properties1 = Object.getOwnPropertyNames(target)
    let properties2 = Object.getOwnPropertyNames(keep)
    let get = false;

    for (let i = 0; i < properties1.length; i++) {
        get = false;
        for (let y = 0; y < properties2.length; y++) {
            if(properties2[y] === properties1[i]) get = true
        }
        if(get === false){
            delete target[properties1[i]]
        }
    }
    return target
}

const config = { user: "user", pass: "pass" };
const tooMuchConfig = { vars: "LOG=info", user: "user", pass: "pass", env: "prod" };

const properConfig = restrict(tooMuchConfig, config);
console.log(properConfig)
assert.deepEqual(properConfig, { user: 'user', pass: 'pass' })
