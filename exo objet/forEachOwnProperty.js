const assert = require("assert")

   Object.prototype.forEachOwnProperty = function(f) {
       let result = [];
        for (let i = 0; i < Object.getOwnPropertyNames(this).length; i++) {
            f(Object.getOwnPropertyNames(this)[i])
        }
    }

const o1 = { a: 1 };
const o2 = Object.create(o1);
o2.b = 2;
o2.c = 3;

const props = [];
o2.forEachOwnProperty(prop => props.push(prop));
console.log(props); // => ["b", "c"]; mais pas "a"
assert.deepEqual(props, [ 'b', 'c' ])
