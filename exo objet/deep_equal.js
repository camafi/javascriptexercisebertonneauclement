const assert = require("assert")

function deepEqual(a1,a2){
    let depth1 = 0;
    let depth2 = 0;

    for (let i = 0; i < a1.length; i++) {
        if(Array.isArray(a1[i])) depth1++
    }
    for (let i = 0; i < a2.length; i++) {
        if(Array.isArray(a2[i])) depth2++
        
    }

    if(depth1 === depth2) return true
    else return false
}

const a1 = [1, 2, [3, 4], 5];
const a2 = JSON.parse(JSON.stringify(a1)); // Deep copy
console.log(a1 === a2); // => false
console.log(deepEqual(a1, a2)); // => true
assert.deepEqual(a1 === a2, false)
assert.deepEqual(deepEqual(a1, a2), true)
