const assert = require("assert")

function init_with(size,f){
    let result = [];
    for(let i =0; i<size; i++){
        result.push(f(i))
    }
    return result
}

const withZero = () => 0;
const fromZero = index => index;
const from42 = index => 42 + index;

console.log(init_with(5,withZero))
console.log(init_with(5,fromZero))
console.log(init_with(5,from42))
assert.deepEqual(init_with(5,withZero), [ 0, 0, 0, 0, 0 ])
assert.deepEqual(init_with(5,fromZero), [ 0, 1, 2, 3, 4 ])
assert.deepEqual(init_with(5,from42), [ 42, 43, 44, 45, 46 ])
